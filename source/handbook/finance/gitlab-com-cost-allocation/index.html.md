---
layout: markdown_page
title: "GitLab.com Cost Allocation"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## GitLab.com Cost Allocation

Below are diagrams that illustrate how various cost items (Infrastructure, Support, Hosting Services) are allocated to GitLab.com.

* [Hosting Allocation](https://drive.google.com/a/gitlab.com/file/d/0B-i7xiLa4PPCZ2JKRkItRmtaV1U/view?usp=sharing)
* [Infrastructure Allocation](https://drive.google.com/a/gitlab.com/file/d/0B-i7xiLa4PPCeWVsRzNNanpfVFE/view?usp=sharing)
* [Support Allocation](https://drive.google.com/a/gitlab.com/file/d/0B-i7xiLa4PPCcDBwcHk1ZDk2a1k/view?usp=sharing)
